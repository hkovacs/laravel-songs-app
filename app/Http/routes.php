<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
$router->bind('songs', function($slug) {
  return App\Song::where('slug', $slug)->first();
});

// Route::get('/', function () {
    // return view('welcome');
// });

// $router is same as Route
// $router->get('/', 'PagesController@index');
// $router->get('about', 'PagesController@about');
// Route::get('songs', 'SongsController@index');
// Route::get('songs/{song}', 'SongsController@show');
// $router->get('songs/{song}/edit', 'SongsController@edit');
// $router->patch('songs/{song}', 'SongsController@update');
// $router->get('music', ['as' => 'songs_path', 'uses' => 'SongsController@index']);
// $router->get('music/{song}', ['as' => 'song_path', 'uses' => 'SongsController@show']);

// Use $router->resource for universal coverage
$router->resource('songs', 'SongsController', [
  'names' => ['index' => 'songs_path', 'show' => 'song_path', 'edit' => 'song_path_edit', 'update' => 'song_path_update'],
  // 'only' => ['index', 'show', 'edit', 'update']
  ]);
// use 'except' to ignore specifically

// $router->resource('people', 'PeopleController', [
  // 'only' => ['index', 'show', 'edit', 'create']
// ]);

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

