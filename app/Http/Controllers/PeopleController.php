<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PeopleController extends Controller
{
    public function index()
    {
      return 'all people';
    }

    public function create()
    {
      return 'create a new person';
    }

    public function show($id)
    {
      return 'show the person with an id of ' .$id;
    }

}
