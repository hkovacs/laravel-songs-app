<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Song;
use App\Http\Requests\CreateSongRequest;

class SongsController extends Controller
{

    private $song;

    public function __construct(Song $song)
    {
      $this->song = $song;
    }

    public function index()
    {
      $songs = $this->song->get();
      // dd($songs);
      return view('songs.index', compact('songs'));
    }

    public function show(Song $song)
    {
      return view('songs.show', compact('song'));
    }

    public function edit(Song $song)
    {
      return view('songs.edit', compact('song'));
    }

    public function update(Song $song, Request $request)
    {
      // dd(\Request::input());

      $song->fill($request->input())->save();

      return redirect('songs');
    }

    public function create()
    {
      return view('songs.create');
    }


    public function store(CreateSongRequest $request, Song $song)
    {
      $song->create($request->all());
      return redirect()->route('songs_path');
    }

    public function destroy(Song $song)
    {
      $song->delete();
      return redirect()->route('songs_path');
    }

}
