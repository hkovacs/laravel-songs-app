<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
      <div class="container">
        <h1>Songs Club</h1>

        @yield('content')

        @yield('footer')
      </div>
    </body>
</html>
