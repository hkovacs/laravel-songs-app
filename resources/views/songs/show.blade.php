@extends('master')

@section('content')

  <h2>{{ $song->title }}</h2>
  {!! link_to_route('song_path_edit', 'Edit ' .$song->title, [$song->slug]) !!}

  @if ($song->lyrics)
    <article class="lyrics">
      {!! nl2br($song->lyrics) !!}
    </article>
  @endif

  {!! link_to_route('songs_path', 'Back to home') !!}

@stop
