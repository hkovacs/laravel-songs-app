@extends('master')

@section('content')

    <h1>Songs</h1>

    @foreach ($songs as $song)
      <li>
        {!! link_to_route('song_path', $song->title, [$song->slug]) !!}
      </li>
    @endforeach

    {!! link_to_route('songs.create', 'Add a new song') !!}

@stop
